package com.bofugroup.service.promotion.bussines.srv;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bofugroup.service.promotion.bussines.dto.Promotion;
import com.bofugroup.service.promotion.exception.ExceptionPromotionNotFound;
import com.bofugroup.service.promotion.repository.PromotionRepository;

@Component
public class PromotionService 
{
	@Autowired
	PromotionRepository promotionRepository;

	
	public Promotion save(Promotion promotion) 
	{
		Promotion response = null;
		if (promotion != null) 
			response = promotionRepository.save(promotion);
		else
			throw new ExceptionPromotionNotFound();
			
		return response; 
	}
	
	public List<Promotion> all()
	{
		return (List<Promotion>) promotionRepository.findAll();
	}
	
	private Optional<Promotion> getId(Long id) 
	{
		return promotionRepository.findById(id);
	}
	


}
